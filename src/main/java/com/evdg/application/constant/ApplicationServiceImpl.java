package com.evdg.application.constant;


import com.evdg.application.config.TokenProvider;
import com.evdg.application.dao.EmployeesRepository;
import com.evdg.application.dao.RolesRepository;
import com.evdg.application.helper.ServiceHelper;
import com.evdg.application.service.JWTServiceImpl;
import com.evdg.application.service.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class ApplicationServiceImpl {


    @Autowired
    public PropertiesServiceImpl propertiesService;
    @Autowired
    public JWTServiceImpl jwtService;
    @Autowired
    public PasswordEncoder bcryptEncoder;
    @Autowired
    public UserDetailsServiceImpl userDetailsService;

    @Autowired
    public TokenProvider jwtTokenUtil;

    @Autowired
    public EmployeesRepository employeesRepository;

    @Autowired
    public RolesRepository rolesRepository;

    @Autowired
    public  TokenProvider tokenProvider;

    @Autowired
    public AuthenticationManager authenticationManager;

    @Autowired
    public ServiceHelper serviceHelper;
}
