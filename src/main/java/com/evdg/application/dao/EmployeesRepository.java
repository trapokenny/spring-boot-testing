package com.evdg.application.dao;

import com.evdg.application.entity.Employees;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EmployeesRepository extends JpaRepository<Employees, Integer> {

    Optional<Employees> findByUsernameEqualsAndStatusEquals(String username, int stat);
}