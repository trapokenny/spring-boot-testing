package com.evdg.application.config;

import com.evdg.application.constant.ApplicationServiceImpl;
import com.evdg.application.entity.Employees;
import com.evdg.application.entity.Roles;
import com.evdg.application.utils.Constants;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
@Component
public class TokenProvider implements Serializable {
    @Autowired
    private ApplicationServiceImpl acs;

    public String getUsernameFromToken(String token) {
        return getClaimFromToken(token, Claims::getSubject);
    }

    public Date getExpirationDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getExpiration);
    }

    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }

    private Claims getAllClaimsFromToken(String token) {
        return Jwts.parser()
                .setSigningKey(Constants.SIGNING_KEY)
                .parseClaimsJws(token)
                .getBody();
    }

    private Boolean isTokenExpired(String token) {
        try {
            String username = acs.jwtService.getUsernameByToken(token);

            if (!(username == null || username.equals("-") || username.equals(""))) {
                return false;
            }
        } catch (Exception e) {
//            e.printStackTrace();
        }
        return true;
    }

    public String generateToken(Authentication authentication) {
        Employees employees = acs.employeesRepository.findByUsernameEqualsAndStatusEquals(authentication.getName(),1).orElse(null);
        final String authorities = authentication.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.joining(","));
        return Jwts.builder()
                .setSubject(authentication.getName())
                .claim(Constants.AUTHORITIES_KEY, authorities)
                .claim("name",employees.getUsername())
                .claim("id",employees.getId())
                .signWith(SignatureAlgorithm.HS256, Constants.SIGNING_KEY)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + Constants.ACCESS_TOKEN_VALIDITY_SECONDS * 1000))
                .compact();
    }

    public Boolean validateToken(String token, UserDetails userDetails) {
        String username = "";
        try {
            username = acs.jwtService.getUsernameByToken(token);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }

    UsernamePasswordAuthenticationToken getAuthentication(final String token, final Authentication existingAuth, final UserDetails userDetails) {
        int ut_id = 0;
        String userName = acs.jwtService.getUsername(token);
        Employees employees = acs.employeesRepository.findByUsernameEqualsAndStatusEquals(userName,1).orElse(null);
        List<Roles> rolesList= acs.rolesRepository.findAllByEid(employees.getId());
        String roles[] = new String[rolesList.size()];

        for (int i = 0; i < rolesList.size(); i++) {
            roles[i] = "ROLE_" + rolesList.get(i).getRole2();
        }

        final Collection<? extends GrantedAuthority> authorities =
                Arrays.stream(roles)
                        .map(SimpleGrantedAuthority::new)
                        .collect(Collectors.toList());

        return new UsernamePasswordAuthenticationToken(userDetails, "", authorities);
    }

}
