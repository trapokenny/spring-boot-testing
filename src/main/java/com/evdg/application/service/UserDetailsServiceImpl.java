package com.evdg.application.service;

import com.evdg.application.constant.ApplicationServiceImpl;
import com.evdg.application.dto.EmployeesDTO;
import com.evdg.application.dto.Model.RequestResult;
import com.evdg.application.entity.Employees;
import com.evdg.application.entity.Roles;
import com.evdg.application.utils.Constants;
import com.google.common.base.Preconditions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
@Service("userDetailsServiceImpl")
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private ApplicationServiceImpl acs;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Optional<Employees>  employeesOptional=acs.employeesRepository.findByUsernameEqualsAndStatusEquals(username,1);

        return new org.springframework.security.core.userdetails.User(employeesOptional.orElse(null).getUsername(), employeesOptional.orElse(null).getPassword(), true, true, true, true, getAuthority(employeesOptional.get().getId()));
    }

    private Set<SimpleGrantedAuthority> getAuthority(int utId) {
        //TODO NEED ADD ROLE
        Set<SimpleGrantedAuthority> authorities = new HashSet<>();
        List<Roles>  rolesList = acs.rolesRepository.findAllByEid(utId);
        rolesList.forEach(roleBean -> {
            authorities.add(new SimpleGrantedAuthority("ROLE_" + roleBean.getRole2()));
        });

        return authorities;
    }

    public Employees getUserBeanByEmailOrUsername(String username) {
        username = username.toLowerCase();
        Optional<Employees> employees = acs.employeesRepository.findByUsernameEqualsAndStatusEquals(username.toLowerCase(),1);
        return employees.get();
    }

    public RequestResult registerUser(EmployeesDTO createUserInfo){
        RequestResult requestResult;
        try{
            Employees employees = new Employees();
            employees.setUsername(createUserInfo.getUsername());
            employees.setPassword( acs.bcryptEncoder.encode(createUserInfo.getPassword()));
            employees.setStatus(1);
            employees =acs.employeesRepository.save(employees);
            Roles roles = new Roles();
            roles.setEid(employees.getId());
            roles.setRole2("USER");
            roles.setUsername(employees.getUsername());
            roles.setStatus(1);
            roles = acs.rolesRepository.save(roles);
            requestResult =  acs.serviceHelper.standardizeWithObject("SUCCESS", "");

        }catch (Exception e){
            e.printStackTrace();
            requestResult =acs.serviceHelper.standardizeWithObject(null, e.getMessage());
        }
        return requestResult;
    }
}
