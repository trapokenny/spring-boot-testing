package com.evdg.application.service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.evdg.application.constant.ApplicationServiceImpl;
import com.evdg.application.entity.Employees;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class JWTServiceImpl {
    Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private ApplicationServiceImpl acs;

    public String getUsernameByToken(String token) throws Exception {
        String userName = getUsername(token);
        Optional<Employees> employees = acs.employeesRepository.findByUsernameEqualsAndStatusEquals(userName, 1);
        return employees.orElse(null).getUsername();
    }

    public String getUsername(String jwtString) {
        try {
            DecodedJWT jwt = JWT.decode(jwtString);
            return jwt.getClaim("name").asString();
        } catch (JWTDecodeException e) {
            throw e;
        }
    }

}
