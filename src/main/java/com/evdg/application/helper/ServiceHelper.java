package com.evdg.application.helper;



import com.evdg.application.dto.Model.RequestResult;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ServiceHelper {
    public RequestResult standardizeWithObject(Object object, String errorMessage) {
        RequestResult requestResult = new RequestResult();

        if (object == null || object.toString().equals("")) {
            requestResult.setMsg("ERROR");
            requestResult.setContent(errorMessage);
        } else {
            Optional<Object> optional = Optional.ofNullable(object);
            requestResult.setMsg("SUCCESS");
            requestResult.setContent(optional.orElseGet(Object::new));
        }

        return requestResult;
    }


}
