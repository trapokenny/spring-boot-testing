package com.evdg.application.entity;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;

@Entity
@Table(name = "roles")
public class Roles {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "username")
    private String username;

    @Column(name = "role2")
    private String role2;

    @Column(name = "e_id")
    private Integer eid;

    @Column(name = "status")
    private Integer status;
    @CreationTimestamp
    @Column(name = "created_datetime")
    private java.sql.Timestamp createdDatetime;
    @UpdateTimestamp
    @Column(name = "updated_datetime")
    private java.sql.Timestamp updatedDatetime;

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRole2() {
        return this.role2;
    }

    public void setRole2(String role2) {
        this.role2 = role2;
    }

    public Integer getEid() {
        return this.eid;
    }

    public void setEid(Integer eid) {
        this.eid = eid;
    }

    public Integer getStatus() {
        return this.status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public java.sql.Timestamp getCreatedDatetime() {
        return this.createdDatetime;
    }

    public void setCreatedDatetime(java.sql.Timestamp createdDatetime) {
        this.createdDatetime = createdDatetime;
    }

    public java.sql.Timestamp getUpdatedDatetime() {
        return this.updatedDatetime;
    }

    public void setUpdatedDatetime(java.sql.Timestamp updatedDatetime) {
        this.updatedDatetime = updatedDatetime;
    }
}
