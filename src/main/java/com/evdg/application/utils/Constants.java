package com.evdg.application.utils;

public class Constants {
    public static final long ACCESS_TOKEN_VALIDITY_SECONDS = 30 * 24 * 60 * 60;
    public static final String SIGNING_KEY = "EVDG@2022@P@ssw0rd??2k22";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String AUTHORITIES_KEY = "scopes";
    //Temporary need change to db get
    public  static final String role="USER";
}
