package com.evdg.application.controller;


import com.evdg.application.constant.ApplicationServiceImpl;
import com.evdg.application.dto.EmployeesDTO;
import com.evdg.application.dto.Model.LoginUser;
import com.evdg.application.dto.TestDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.activation.MimetypesFileTypeMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.util.Map;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/test")
public class TestController {
    private static final Logger logger = LoggerFactory.getLogger(TestController.class);


    @Autowired
    private ApplicationServiceImpl acs;

    @GetMapping("/test")
    public String test() {
       return  "success";
    }

    @Operation(summary="Generate access token (User) (username: admin & password: Qazwsx123)",method="POST",responses = {
            @ApiResponse(responseCode  = "1", description  = ""),
            @ApiResponse(responseCode  = "200", description  = ""),
            @ApiResponse(responseCode  = "401", description  = "")} )
    @PostMapping(value = "/generate-token",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> generateToken(@Valid @RequestBody LoginUser loginUser, HttpServletRequest servletRequest, HttpServletResponse response) throws AuthenticationException, IOException {
        final Authentication authentication = acs.authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginUser.getUsername(),
                        loginUser.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String token = acs.tokenProvider.generateToken(authentication);
        return ResponseEntity.ok(acs.serviceHelper.standardizeWithObject(token, ""));
    }

    @PostMapping("/create-employee-account")
    public ResponseEntity<?> createUserAccount(@RequestBody @Valid EmployeesDTO employeesDTO, BindingResult results, Authentication authentication) {
        if (results.hasErrors()) {
            return ResponseEntity.ok(ResponseEntity.badRequest());
        }
        return ResponseEntity.ok(acs.userDetailsService.registerUser(employeesDTO));
    }


    @RequestMapping(value = "/web/action/test6", method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<?> test(@ModelAttribute TestDto map) {

        System.out.println("firstname-"+map.getFirstName());
        System.out.println("lastName-"+map.getLastName());
        return ResponseEntity.ok("success");
    }

}
