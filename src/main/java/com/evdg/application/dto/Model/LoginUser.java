package com.evdg.application.dto.Model;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@Schema(description = "Login Information (User)")
public class LoginUser {

    @Schema(description = "Username / Email",required = true)
    @NotNull(message = "Username cannot be empty")
    private String username;
    @Schema(description = "Password",required = true)
    @NotNull(message = "Password cannot be empty")
    private String password;

}
