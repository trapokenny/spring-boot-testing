package com.evdg.application.dto;

public class RolesDTO {
    private Integer id;
    private String username;
    private String role2;
    private Integer eId;
    private Integer status;
    private java.sql.Timestamp createdDatetime;
    private java.sql.Timestamp updatedDatetime;

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRole2() {
        return this.role2;
    }

    public void setRole2(String role2) {
        this.role2 = role2;
    }

    public Integer getEId() {
        return this.eId;
    }

    public void setEId(Integer eId) {
        this.eId = eId;
    }

    public Integer getStatus() {
        return this.status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public java.sql.Timestamp getCreatedDatetime() {
        return this.createdDatetime;
    }

    public void setCreatedDatetime(java.sql.Timestamp createdDatetime) {
        this.createdDatetime = createdDatetime;
    }

    public java.sql.Timestamp getUpdatedDatetime() {
        return this.updatedDatetime;
    }

    public void setUpdatedDatetime(java.sql.Timestamp updatedDatetime) {
        this.updatedDatetime = updatedDatetime;
    }
}
