package com.evdg.application.dto;

public class EmployeesDTO {
    private Integer id;
    private String username;
    private String password;
    private Integer status;
    private java.sql.Timestamp createdDatetime;
    private java.sql.Timestamp updatedDatetime;

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getStatus() {
        return this.status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public java.sql.Timestamp getCreatedDatetime() {
        return this.createdDatetime;
    }

    public void setCreatedDatetime(java.sql.Timestamp createdDatetime) {
        this.createdDatetime = createdDatetime;
    }

    public java.sql.Timestamp getUpdatedDatetime() {
        return this.updatedDatetime;
    }

    public void setUpdatedDatetime(java.sql.Timestamp updatedDatetime) {
        this.updatedDatetime = updatedDatetime;
    }
}
